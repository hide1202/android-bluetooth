package com.nhnent.example.bluetooth;

import android.bluetooth.BluetoothClass;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class BluetoothClassName {
    static final Map<Integer, String> mMajorClassTable = new HashMap<>();
    static final Map<Integer, String> mDeviceClassTable = new HashMap<>();

    static {
        Field[] deviceFields = BluetoothClass.Device.class.getFields();
        for (Field f : deviceFields) {
            try {
                int o = (int) f.get(null);
                mDeviceClassTable.put(o, f.getName());
            } catch (IllegalAccessException e) {
            }
        }

        Field[] majorFields = BluetoothClass.Device.Major.class.getFields();
        for (Field f : majorFields) {
            try {
                int o = (int) f.get(null);
                mMajorClassTable.put(o, f.getName());
            } catch (IllegalAccessException e) {
            }
        }
    }

    public static String getDeviceClassName(int i) {
        return mDeviceClassTable.get(i);
    }

    public static String getMajorClassName(int i) {
        return mMajorClassTable.get(i);
    }

}
