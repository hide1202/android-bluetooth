package com.nhnent.example.bluetooth;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private TextView mResultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mResultTextView = findViewById(R.id.resultTextView);
        findViewById(R.id.logBluetoothBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONArray bondedDevices = BluetoothHelper.getBondedDevices();
                if (bondedDevices == null) {
                    String errorMsg = "Can't read bluetooth because of 'not supported' or 'disable'";
                    mResultTextView.setText(errorMsg);
                    Log.w(TAG, errorMsg);
                } else {
                    try {
                        mResultTextView.setText(bondedDevices.toString(2));
                        Log.i(TAG, bondedDevices.toString(4));
                    } catch (JSONException ignored) {
                    }
                }
            }
        });
    }
}
