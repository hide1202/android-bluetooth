package com.nhnent.example.bluetooth;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.os.Build;
import android.os.ParcelUuid;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

public class BluetoothHelper {
    private static final String TAG = BluetoothHelper.class.getSimpleName();

    private BluetoothHelper() {
    }

    /**
     * Get json array that have bonded bluetooth devices in device. MUST require {@link Manifest.permission#BLUETOOTH} permission.
     * if the application doesn't have required permission, crash the application.
     *
     * @return json array that describes bonded bluetooth devices. if couldn't support bluetooth, or disable bluetooth return null.
     */
    @RequiresPermission(Manifest.permission.BLUETOOTH)
    public static
    @Nullable
    JSONArray getBondedDevices() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            return null;
        }
        if (!bluetoothAdapter.isEnabled()) {
            return null;
        }

        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
        JSONArray result = new JSONArray();
        for (BluetoothDevice d : bondedDevices) {
            JSONObject deviceJson = from(d);
            if (deviceJson != null) {
                result.put(deviceJson);
            }
        }

        return result;
    }

    /**
     * Get json array that have bonded bluetooth devices in device. SHOULD require {@link Manifest.permission#BLUETOOTH} permission.
     * if the application doesn't have required permission, return null.
     *
     * @return json array that describes bonded bluetooth devices. if couldn't support bluetooth, or disable bluetooth return null.
     */
    @RequiresPermission(value = Manifest.permission.BLUETOOTH, conditional = true)
    public static
    @Nullable
    JSONArray getSafeBondedDevices() {
        try {
            return getBondedDevices();
        } catch (SecurityException e) {
            Log.w(TAG, "SHOULD require permission : android.permission.BLUETOOTH");
            return null;
        }
    }

    @RequiresPermission(Manifest.permission.BLUETOOTH)
    private static
    @Nullable
    JSONObject from(BluetoothDevice device) {
        try {
            JSONObject json = new JSONObject();
            json.put("name", device.getName());
            if (Build.VERSION.SDK_INT >= 18) {
                json.put("type", device.getType());
            } else {
                // BluetoothDevice.DEVICE_TYPE_UNKNOWN = 0
                json.put("type", 0);
            }
            json.put("address", device.getAddress());

            JSONObject bluetoothClassJson = new JSONObject();
            BluetoothClass bluetoothClass = device.getBluetoothClass();
            bluetoothClassJson.put("majorDeviceClass", bluetoothClass.getMajorDeviceClass());
            bluetoothClassJson.put("deviceClass", bluetoothClass.getDeviceClass());

            json.put("bluetoothClass", bluetoothClassJson);
            json.put("bondState", device.getBondState());

            ParcelUuid[] uuids = device.getUuids();
            JSONArray uuidsJson = new JSONArray();
            if (uuids != null) {
                for (ParcelUuid uuid : uuids) {
                    if (uuid != null) {
                        uuidsJson.put(uuid.getUuid().toString());
                    }
                }
            }
            json.put("uuids", uuidsJson);
            return json;
        } catch (JSONException e) {
            return null;
        }
    }
}
